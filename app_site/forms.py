from django import forms
from .models import Subcriber
from django.utils.translation import ugettext_lazy as _


class SubcriberForm(forms.ModelForm):

    class Meta:
        model = Subcriber
        exclude = ()
