from django.utils.translation import ugettext_lazy as _
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.edit import CreateView


from .models import Subcriber
from .forms import SubcriberForm

class IndexPageSubscriber(SuccessMessageMixin, CreateView):
    model = Subcriber
    form_class = SubcriberForm
    success_url = "/"
    success_message = _("Thank You for your subscription")
