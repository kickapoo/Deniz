from django.contrib import admin
from .models import Subcriber

class SubAdmin(admin.ModelAdmin):
    pass

admin.site.register(Subcriber, SubAdmin)
